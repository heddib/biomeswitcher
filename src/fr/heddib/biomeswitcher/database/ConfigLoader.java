package fr.heddib.biomeswitcher.database;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;

import fr.heddib.biomeswitcher.BiomeSwitcher;

public class ConfigLoader {
	
	public void load() {
		reloadConfig();
	}

	public static void reloadConfig() {
		BiomeSwitcher.getInstance().reloadConfig();
		FileConfiguration c = BiomeSwitcher.getInstance().getConfig();
		
		c.options().header(
				"BiomeSwitcher: Transformer les biomes avec d'autres biomes !" +
				"\n" +
				"Pour transformer tous les biomes, utilisez: ALL;<biome>" +
				"\n" +
				"Pour transformer le biome 1 en biome 2, utilisez: <biome1>;<biome2>" + 
				"\n" + 
				"Petite liste des biomes:  OCEAN,  PLAINS,  DESERT,  EXTREME_HILLS,  FOREST,  TAIGA,  SWAMPLAND,  RIVER,  HELL,  SKY,  FROZEN_OCEAN,  FROZEN_RIVER,  ICE_FLATS,  ICE_MOUNTAINS,  MUSHROOM_ISLAND,  MUSHROOM_ISLAND_SHORE,  BEACHES,  DESERT_HILLS,  FOREST_HILLS,  TAIGA_HILLS,  SMALLER_EXTREME_HILLS,  JUNGLE,  JUNGLE_HILLS,  JUNGLE_EDGE,  DEEP_OCEAN,  STONE_BEACH,  COLD_BEACH,  BIRCH_FOREST,  BIRCH_FOREST_HILLS,  ROOFED_FOREST,  TAIGA_COLD,  TAIGA_COLD_HILLS,  REDWOOD_TAIGA,  REDWOOD_TAIGA_HILLS,  EXTREME_HILLS_WITH_TREES,  SAVANNA,  SAVANNA_ROCK,  MESA,  MESA_ROCK,  MESA_CLEAR_ROCK"
				);
		
		List<String> biomes = new ArrayList<String>();
		biomes.add("OCEAN;PLAINS");
		c.addDefault("Biomes", biomes);
		
		c.options().copyDefaults(true);
		BiomeSwitcher.getInstance().saveConfig();
	}

}
