package fr.heddib.biomeswitcher.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;

import org.apache.commons.lang3.Validate;

import fr.heddib.biomeswitcher.BiomeSwitcher;
import net.minecraft.server.v1_9_R1.BiomeBase;
import net.minecraft.server.v1_9_R1.MinecraftKey;
import net.minecraft.server.v1_9_R1.RegistryID;
import net.minecraft.server.v1_9_R1.RegistryMaterials;

public class BiomeRegistry extends RegistryMaterials<MinecraftKey, BiomeBase> implements IRegistry<MinecraftKey, BiomeBase> {
	
    protected final Map<MinecraftKey, BiomeBase> registry = new HashMap<MinecraftKey, BiomeBase>();
    protected final IDRegistry<BiomeBase> idRegistry = new IDRegistry<BiomeBase>(256);
    public static String[] allbiomes = { "OCEAN", "PLAINS", "DESERT", "EXTREME_HILLS", "FOREST", "TAIGA", "SWAMPLAND", "RIVER", "HELL", "SKY", "FROZEN_OCEAN", "FROZEN_RIVER", "ICE_FLATS", "ICE_MOUNTAINS", "MUSHROOM_ISLAND", "MUSHROOM_ISLAND_SHORE", 
                                         "BEACHES", "DESERT_HILLS", "FOREST_HILLS", "TAIGA_HILLS", "SMALLER_EXTREME_HILLS", "JUNGLE", "JUNGLE_HILLS", "JUNGLE_EDGE", "DEEP_OCEAN", "STONE_BEACH", "COLD_BEACH", "BIRCH_FOREST", "BIRCH_FOREST_HILLS", 
                                         "ROOFED_FOREST", "TAIGA_COLD", "TAIGA_COLD_HILLS", "REDWOOD_TAIGA", "REDWOOD_TAIGA_HILLS", "EXTREME_HILLS_WITH_TREES", "SAVANNA", "SAVANNA_ROCK", "MESA", "MESA_ROCK", "MESA_CLEAR_ROCK" };

    public static BiomeRegistry getInstance() {
        if (!(BiomeBase.REGISTRY_ID instanceof BiomeRegistry)) {        	
            init();
        }
        return (BiomeRegistry)BiomeBase.REGISTRY_ID;
    }

    public void a(int id, MinecraftKey entry, BiomeBase value) {
        register(id, entry, value);
    }

    public void a(MinecraftKey entry, BiomeBase value) {
        Validate.notNull(entry);
        Validate.notNull(value);
        if (this.registry.containsKey(entry)) {
            BiomeSwitcher.getInstance().getLogger().log(Level.FINE, "Adding duplicate key '" + entry + "' to registry");
        }
        this.registry.put(entry, value);
    }

    public void register(int id, MinecraftKey entry, BiomeBase value) {
    	BiomeSwitcher.getInstance().getLogger().log(Level.FINE, "Enregistrement de: " + value.l());
        this.idRegistry.put(value, Integer.valueOf(id));
        a(entry, value);
    }

    public BiomeBase get(MinecraftKey var1) {
       return getObject(var1);
    }

    public MinecraftKey b(BiomeBase var1) {
       return getNameForObject(var1);
    }

    public boolean d(MinecraftKey var1) {
       return containsKey(var1);
    }

    public int a(BiomeBase var1) {
       return getIDForObject(var1);
    }

    public BiomeBase getId(int var1) {
       return getObjectById(var1);
    }

    public BiomeBase getObject(MinecraftKey name) {
       return (BiomeBase)this.registry.get(name);
    }

    public MinecraftKey getNameForObject(BiomeBase obj) {   
        MinecraftKey result = null;
        for (Map.Entry<MinecraftKey, BiomeBase> entryset : this.registry.entrySet()) {
            if ((entryset.getValue() != null) && (((BiomeBase)entryset.getValue()).equals(obj))) {       
                if (result != null) {
                   BiomeSwitcher.getInstance().getLogger().log(Level.WARNING, "DUPLICATE ENTRY FOR BIOME " + obj.getClass().getName());
                   break;
                }
                result = (MinecraftKey)entryset.getKey();
            }
        }
        return result;
    }

    public boolean containsKey(MinecraftKey key) {
        return this.registry.containsKey(key);
    }

    public int getIDForObject(BiomeBase obj) {
        return this.idRegistry.get(obj);
    }

    public BiomeBase getObjectById(int id) {
        return (BiomeBase)this.idRegistry.getByValue(id);
    }

    public Iterator<BiomeBase> iterator() {
        return this.idRegistry.iterator();
    }

    @SuppressWarnings("unchecked")
    public static boolean init() {
        try {       	
            BiomeRegistry registry = new BiomeRegistry();
            RegistryMaterials<MinecraftKey, BiomeBase> oldRegistry = BiomeBase.REGISTRY_ID;
            RegistryID<BiomeBase> oldIDRegistry = (RegistryID<BiomeBase>)ReflectionUtils.getValue(RegistryMaterials.class, oldRegistry, "a");
            Map<BiomeBase, MinecraftKey> oldDataRegistry = (Map<BiomeBase, MinecraftKey>)ReflectionUtils.getValue(RegistryMaterials.class, oldRegistry, "b");
            for (Map.Entry<BiomeBase, MinecraftKey> entry : oldDataRegistry.entrySet()) {        
                int id = oldIDRegistry.getId((BiomeBase)entry.getKey());
                if ((id != -1) && (entry.getKey() != null)) {              	
                    registry.register(id, (MinecraftKey)entry.getValue(), (BiomeBase)entry.getKey());
                }
            }
            ReflectionUtils.setFinalStatic(BiomeBase.class.getDeclaredField("REGISTRY_ID"), registry);
       } catch (ReflectiveOperationException e) {
            e.printStackTrace();
            return false;
       }
       return true;
   }
}
