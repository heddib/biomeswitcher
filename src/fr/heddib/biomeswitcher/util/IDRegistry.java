package fr.heddib.biomeswitcher.util;

import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;

import com.google.common.base.Predicates;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;

public class IDRegistry<T> implements Iterable<T> {
	
    private final IdentityHashMap<T, Integer> identityMap;
	private final List<T> objectList = Lists.newArrayList();
	  
	public IDRegistry(int size) {
	    this.identityMap = new IdentityHashMap<T, Integer>(size);
	}
	  
	public void put(T key, Integer value) {
	    this.identityMap.put(key, value);
	    while (this.objectList.size() <= value.intValue()) {
	      this.objectList.add(null);
	    }
	    this.objectList.set(value.intValue(), key);
	}
	  
	public void remove(T key) {
	    put(key, null);
	}
	  
	public int get(T key) {
	    Integer integer = (Integer)this.identityMap.get(key);
	    return integer == null ? -1 : integer.intValue();
	}
	  
	public final T getByValue(int value) {
	    return (value >= 0) && (value < this.objectList.size()) ? this.objectList.get(value) : null;
	}
	  
	public Iterator<T> iterator() {
	    return Iterators.filter(this.objectList.iterator(), Predicates.notNull());
	}

}
