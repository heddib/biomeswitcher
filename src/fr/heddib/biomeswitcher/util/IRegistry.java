package fr.heddib.biomeswitcher.util;

public interface IRegistry<K, V> extends Iterable<V> {

	public abstract void register(int paramInt, K paramK, V paramV);
	  
    public abstract V getObject(K paramK);
	  
	public abstract K getNameForObject(V paramV);
	  
	public abstract boolean containsKey(K paramK);
	  
	public abstract int getIDForObject(V paramV);
	  
	public abstract V getObjectById(int paramInt);
	
}
