package fr.heddib.biomeswitcher;

import java.util.List;

import net.minecraft.server.v1_9_R1.MinecraftKey;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import fr.heddib.biomeswitcher.database.ConfigLoader;
import fr.heddib.biomeswitcher.util.BiomeID;
import fr.heddib.biomeswitcher.util.BiomeRegistry;

/**
 * BiomeSwitcher
 * @author heddib
 *
 */
public class BiomeSwitcher extends JavaPlugin {
	
	private static BiomeSwitcher i;
	public static BiomeSwitcher getInstance() {return i;}
	
	public void onEnable() {
		i = this;
		getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "BiomeSwitcher v" + getDescription().getVersion() + " par heddib...");
		new ConfigLoader().load();
		
		BiomeRegistry registry = BiomeRegistry.getInstance();
	    List<String> biomes = getConfig().getStringList("Biomes");
	    for (String biome : biomes) {		    	
	        String[] switcher = biome.split(";");
	        if (switcher[0].equals("ALL")) {
	            for (int i = 0; i < BiomeRegistry.allbiomes.length; i++) {
	                registry.register(i, new MinecraftKey(BiomeRegistry.allbiomes[i].toLowerCase()), registry.getObject(new MinecraftKey(switcher[1].toLowerCase())));
	                getServer().getConsoleSender().sendMessage(ChatColor.YELLOW + "Transformaton de " + BiomeRegistry.allbiomes[i] + " avec " + switcher[1]);
	            }
	        } else {			        	
	            int oldbiomeid = BiomeID.valueOf(switcher[0]).getId();
	            registry.register(oldbiomeid, new MinecraftKey(switcher[0].toLowerCase()), registry.getObject(new MinecraftKey(switcher[1].toLowerCase())));
	            getServer().getConsoleSender().sendMessage(ChatColor.YELLOW + "Transformaton de " + switcher[0] + " avec " + switcher[1]);
	        }
	    }
	    getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "Termine.");
	}
	
	public void onDisable() {
		getServer().getConsoleSender().sendMessage(ChatColor.RED + "BiomeSwitcher v" + getDescription().getVersion() + " par heddib...");
	}

}
